package interfaceExample;

public interface Human {
    public void dancing();
    public void sleeping(int hour);
    public void working(String position);
}
