package interfaceExample;

public class Fireman implements Human{
    public Fireman() {
    }

    @Override
    public void dancing() {

    }

    @Override
    public void sleeping(int hour) {
        System.out.println("სძინავს "+hour+" საათი");
    }

    @Override
    public void working(String position) {
        System.out.println("მუშაობს "+position+"-დ");
    }
}
