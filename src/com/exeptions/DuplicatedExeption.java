package com.exeptions;

public class DuplicatedExeption extends Exception {
    public DuplicatedExeption() {
    }

    public DuplicatedExeption(String message) {
        super(message);
    }
}
